<br/>
<div align="center">

  # Proxyless Spotify Follow Bot
  
<h1>

  Requests based multi-threaded script for increasing followers on Spotify. Click <a href="https://github.com/Gonehe/Proxyless-Spotify-Follow-Bot/issues">here</a> to report bugs.
  
  ![image](https://user-images.githubusercontent.com/102762968/161622560-22f88eae-709e-4c07-a66e-8147dd3d43dd.png)

</div>

--------------------------------------

### Usage

1. Install requirements.txt</a> by typing `pip install -r requirements.txt` in Command Prompt
2. Run the `run.bat` file and enter the amount of threads

--------------------------------------

### Please note

This script was made for educational purposes, I am not responsible for your actions using this script. This script was made for educational purposes.
